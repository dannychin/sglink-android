package com.intewealth.app.sglink.Bus;

import android.content.Context;
import android.graphics.Bitmap;
import android.util.AttributeSet;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.intewealth.app.sglink.R;
import com.intewealth.app.sglink.Utils.L;

/**
 * User: Pavel
 * Date: 07.12.2016
 * Time: 20:14
 */

public class BusStopMarkerClusterView extends FrameLayout {

    /************************************
     * BINDS
     ************************************/
    @BindView(R.id.title)
    TextView title;

    /************************************
     * PUBLIC METHODS
     ************************************/
    public BusStopMarkerClusterView(Context context) {
        super(context);
        init();
    }

    public BusStopMarkerClusterView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public BusStopMarkerClusterView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    public BusStopMarkerClusterView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init();
    }

    public Bitmap render() {
        L.i("render");
        setDrawingCacheEnabled(true);
        measure(0, 0);
        layout(0, 0, getMeasuredWidth(), getMeasuredHeight());
        buildDrawingCache();
        return getDrawingCache();
    }

    public void setTitle(String title) {
        L.i("setTitle");
        this.title.setText(title);
        invalidate();
    }

    /************************************
     * PRIVATE METHODS
     ************************************/
    private void init() {
        L.i("init");
        inflate(getContext(), R.layout.view_bus_stop_marker_cluster, this);
        ButterKnife.bind(this);
    }

}
