package com.intewealth.app.sglink.Utils;

import android.content.Context;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.RetryPolicy;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.Volley;
import com.intewealth.app.sglink.BuildConfig;
import com.intewealth.app.sglink.config.Settings;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.otto.Bus;

import timber.log.Timber;

import android.support.annotation.NonNull;
import android.support.multidex.MultiDex;
import android.support.multidex.MultiDexApplication;

/**
 * Global application singleton instance.
 */
public class App extends MultiDexApplication {

    /************************************
     * PRIVATE FIELDS
     ************************************/
    // Singleton application sInstance
    private static App sInstance;
    // Volley request queue
    private RequestQueue mRequestQueue;
    // Volley image cache
    private LruBitmapCache mLruBitmapCache;
    // Volley image loader
    private ImageLoader mImageLoader;
    //Otto Bus
    private Bus mOttoBus;

    @Override
    protected void attachBaseContext(Context base) {
        L.i("attachBaseContext");
        super.attachBaseContext(base);
        MultiDex.install(this);
    }

    @Override
    public void onCreate() {
        L.i("onCreate");
        super.onCreate();

        if (BuildConfig.DEBUG) {
            Timber.plant(new Timber.DebugTree());
            L.plant(new L.DebugTree());
        }

        Settings.init(this);

        sInstance = this;
    }

    /**
     * @return the application singleton instance
     */
    public static App getInstance() {
        L.i("getInstance");
        return sInstance;
    }

    /**
     * Returns a Volley request queue for creating network requests
     *
     * @return {@link com.android.volley.RequestQueue}
     */
    public RequestQueue getVolleyRequestQueue() {
        L.i("getVolleyRequestQueue");
        if (mRequestQueue == null) {
            mRequestQueue = Volley.newRequestQueue(this, new OkHttpStack(new OkHttpClient()));
        }
        return mRequestQueue;
    }

    /**
     * Adds a request to the Volley request queue
     *
     * @param request to be added to the Volley requests queue
     */
    private static void addRequest(@NonNull final Request<?> request) {
        L.i("addRequest");
        RequestQueue requestQueue = getInstance().getVolleyRequestQueue();
        int socketTimeout = 15000; // 15 seconds
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        request.setRetryPolicy(policy);
        requestQueue.add(request);
    }

    /**
     * Adds a request to the Volley request queue with a given tag
     *
     * @param request is the request to be added
     * @param tag     tag identifying the request
     */
    public static void addRequest(@NonNull final Request<?> request, @NonNull final String tag) {
        L.i("addRequest");
        request.setTag(tag);
        addRequest(request);
    }

    /**
     * Cancels all the request in the Volley queue for a given tag
     *
     * @param tag associated with the Volley requests to be cancelled
     */
    public static void cancelAllRequests(@NonNull final String tag) {
        L.i("cancelAllRequests");
        if (getInstance().getVolleyRequestQueue() != null) {
            getInstance().getVolleyRequestQueue().cancelAll(tag);
        }
    }

    /**
     * Returns an image loader instance to be used with Volley.
     *
     * @return {@link com.android.volley.toolbox.ImageLoader}
     */
    public ImageLoader getVolleyImageLoader() {
        L.i("getVolleyImageLoader");
        if (null == mImageLoader) {
            mImageLoader = new ImageLoader(
                    getVolleyRequestQueue(),
                    App.getInstance().getVolleyImageCache());
        }

        return mImageLoader;
    }

    public Bus getOttoBus() {
        L.i("getOttoBus");
        if (null == mOttoBus) {
            mOttoBus = new Bus();
        }

        return mOttoBus;
    }

    /**
     * Returns a bitmap cache to use with volley.
     *
     * @return {@link LruBitmapCache}
     */
    private LruBitmapCache getVolleyImageCache() {
        L.i("getVolleyImageCache");
        if (mLruBitmapCache == null) {
            mLruBitmapCache = new LruBitmapCache(sInstance);
        }
        return mLruBitmapCache;
    }


}