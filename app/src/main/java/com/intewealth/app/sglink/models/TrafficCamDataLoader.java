package com.intewealth.app.sglink.models;

import android.util.Log;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.intewealth.app.sglink.Utils.App;
import com.intewealth.app.sglink.Utils.AppConstant;
import com.intewealth.app.sglink.Utils.L;
import com.intewealth.app.sglink.Utils.Util;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Danny on 25/8/16
 */
public class TrafficCamDataLoader extends DataLoader {

    /************************************
     * PRIVATE STATIC CONSTANTS
     ************************************/
    public final String CAMERAS_BASE_URL = AppConstant.API_BASE_URL + "/api/traffic";
    public final String REQUEST_TAG = "TRAFFIC_API";
    public final String DATA_TAG = "results";

    /************************************
     * PRIVATE STATIC FIELDS
     ************************************/
    private static TrafficCamDataLoader instance;

    /************************************
     * PUBLIC STATIC METHODS
     ************************************/
    public static TrafficCamDataLoader getInstance() {
        if (instance == null) {
            instance = new TrafficCamDataLoader();
        }
        return instance;
    }

    /************************************
     * PUBLIC METHODS
     ************************************/
    public void loadTrafficCamera() {
        L.i("loadTrafficCamera");
        JsonObjectRequest jsonRequest = new JsonObjectRequest(Request.Method.GET, CAMERAS_BASE_URL, "",
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        L.i("onResponse", "traffic: On Received!");
                        processTrafficResult(response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                        L.i("onErrorResponse", "error:" + error.toString());
                    }
                }
        );
        App.addRequest(jsonRequest, REQUEST_TAG);
    }

    public void processTrafficResult(JSONObject jsonResponse) {

        Map<String, ArrayList<Camera>> cameras = null;

        L.i("processTrafficResult", jsonResponse);
        try {
            ArrayList<Camera> cameraArr = new ArrayList<>();
            JSONArray data = (JSONArray) jsonResponse.get(DATA_TAG);
            for (int i = 0; i < data.length(); i++) {
                JSONObject cameraData = (JSONObject) data.get(i);
                Camera camera = new Camera(Util.parseJsonString(cameraData, "camera"));
                camera.setImageURL(Util.parseJsonString(cameraData, "path"));
                camera.setRoute(Util.parseJsonString(cameraData, "route"));
                camera.setLabel(Util.parseJsonString(cameraData, "label"));
                camera.setDate(Util.parseJsonString(cameraData, "date"));

                cameraArr.add(camera);
            }

            ArrayList<Camera> causwayCamera = new ArrayList<>();
            ArrayList<Camera> link2Cameara = new ArrayList<>();
            for (int i = 0; i < cameraArr.size(); i++) {
                if (cameraArr.get(i).getRoute().equals(Camera.ROUTE_CAUSEWAY)) {
                    causwayCamera.add(cameraArr.get(i));
                } else if (cameraArr.get(i).getRoute().equals(Camera.ROUTE_LINK2)) {
                    link2Cameara.add(cameraArr.get(i));
                }
            }

            cameras = new HashMap<>();
            cameras.put(Camera.ROUTE_CAUSEWAY, causwayCamera);
            cameras.put(Camera.ROUTE_LINK2, link2Cameara);

        } catch (JSONException e) {
            L.e("processTrafficResult - JSONException - " + Log.getStackTraceString(e));
        } finally {
            L.i("processTrafficResult - post cameras" + (cameras != null ? cameras.toString() : "null"));
            if (cameras != null) {
                App.getInstance().getOttoBus().post(cameras);
            }
        }

    }

    /************************************
     * PRIVATE METHODS
     ************************************/
    private TrafficCamDataLoader(){
        L.i("TrafficCamDataLoader");
    }

}
