package com.intewealth.app.sglink.managers;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import com.intewealth.app.sglink.Utils.BitmapUtils;
import com.intewealth.app.sglink.Utils.IOUtil;
import com.intewealth.app.sglink.Utils.L;
import com.intewealth.app.sglink.models.Camera;

import java.io.File;

/**
 * User: Pavel
 * Date: 29.10.2016
 * Time: 12:22
 */
public class ImagesManager {

    /************************************
     * PUBLIC STATIC METHODS
     ************************************/
    public static final String PNG_EXTENSION = ".png";

    /************************************
     * PUBLIC STATIC METHODS
     ************************************/
    public static void saveImage(Context context, Camera camera, Bitmap picture) {
        L.i("saveImage", camera.getName(), camera.getImageURL());
        /**
         * TODO:
         * Pavel:
         * need to do it in the background thread
         */
        IOUtil.savePicDefault(context, picture, convertCameraNameToFileName(camera) + PNG_EXTENSION);
    }

    public static boolean imageSaved(Context context, Camera camera) {
        L.i("imageSaved");
        return IOUtil.isFileExist(getFilePath(context, camera.getName()));
    }

    public static boolean imageSaved(Context context, String cameraName) {
        L.i("imageSaved", cameraName);
        return IOUtil.isFileExist(getFilePath(context, cameraName));
    }

    public static Uri getImageUri(Context context, Camera camera) {
        L.i("getImageUri");
        return getImageUri(context, camera.getName());
    }

    public static Uri getImageUri(Context context, String cameraName) {
        L.i("getImageUri", cameraName, Uri.fromFile(new File(getFilePath(context, cameraName))).toString());
        return Uri.fromFile(new File(getFilePath(context, cameraName)));
    }

    /************************************
     * PRIVATE STATIC METHODS
     ************************************/
    private static String getFilePath(Context context, String cameraName) {
        L.i("getFilePath", cameraName);
        return IOUtil.getDataStoragePath(context) + File.separator + cameraName + PNG_EXTENSION;
    }

    private static String convertCameraNameToFileName(Camera camera) {
        L.i("convertCameraNameToFileName");
        return convertCameraNameToFileName(camera.getName());
    }

    private static String convertCameraNameToFileName(String cameraName) {
        L.i("convertCameraNameToFileName");
        return cameraName.toLowerCase().replace(" ","_").replace("/", "_").replace(".", "_");
    }

}
