package com.intewealth.app.sglink.models;

import android.util.Log;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.intewealth.app.sglink.Utils.App;
import com.intewealth.app.sglink.Utils.AppConstant;
import com.intewealth.app.sglink.Utils.L;
import com.intewealth.app.sglink.Utils.Util;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Danny on 25/8/16
 */
public class BusDataLoader extends DataLoader {

    /************************************
     * PRIVATE STATIC CONSTANTS
     ************************************/
    public final String BUSES_BASE_URL = AppConstant.API_BASE_URL + "/api/buses";
    public final String REQUEST_TAG = "BUSES_API";
    public final String RESULTS = "results";

    /************************************
     * PRIVATE STATIC FIELDS
     ************************************/
    private static BusDataLoader instance;

    /************************************
     * PUBLIC STATIC METHODS
     ************************************/
    public static BusDataLoader getInstance() {
        if (instance == null) {
            instance = new BusDataLoader();
        }
        return instance;
    }

    /************************************
     * PUBLIC METHODS
     ************************************/
    public void loadBuses() {
        L.i("loadBuses");
        JsonObjectRequest jsonRequest = new JsonObjectRequest(Request.Method.GET, BUSES_BASE_URL, "",
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        L.i("loadBuses - onResponse", "traffic: On Received!");
                        processBusesResult(response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                        L.i("loadBuses - onErrorResponse", "error:" + error.toString());
                    }
                }
        );
        App.addRequest(jsonRequest, REQUEST_TAG);
    }

    /************************************
     * PRIVATE METHODS
     ************************************/
    private void processBusesResult(JSONObject jsonResponse) {

        List<Bus> buses = new ArrayList<>();

        L.i("processBusesResult", jsonResponse);

        try {
            JSONArray data = (JSONArray) jsonResponse.get(RESULTS);
            for (int i = 0; i < data.length(); i++) {
                JSONObject busData = (JSONObject) data.get(i);
                Bus bus = new Bus();
                bus.description = Util.parseJsonString(busData, Bus.DESCRIPTION);
                bus.stopLat = Util.parseJsonString(busData, Bus.STOP_LAT);
                bus.stopLon = Util.parseJsonString(busData, Bus.STOP_LON);
                bus.busStopId = Util.parseJsonString(busData, Bus.BUS_STOP_ID);
                bus.serviceNumber = Util.parseJsonString(busData, Bus.SERVICE_NUMBER);
                bus.estimatedArrival = Util.parseJsonString(busData, Bus.ESTIMATED_ARRIVAL);
                bus.latitude = Util.parseJsonString(busData, Bus.LATITUDE);
                bus.longitude = Util.parseJsonString(busData, Bus.LONGITUDE);
                bus.visitNumber = Util.parseJsonString(busData, Bus.VISIT_NUMBER);
                bus.sequence = Util.parseJsonString(busData, Bus.SEQUENCE);
                buses.add(bus);
            }
        } catch (JSONException e) {
            L.e("processBusesResult - JSONException - " + Log.getStackTraceString(e));
        } finally {
            L.i("processBusesResult", buses);
            App.getInstance().getOttoBus().post(buses);
        }

    }

    private BusDataLoader() {
        L.i("TrafficCamDataLoader");
    }

}
