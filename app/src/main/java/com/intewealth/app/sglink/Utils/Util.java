package com.intewealth.app.sglink.Utils;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.util.Log;
import android.util.TypedValue;
import org.json.JSONException;
import org.json.JSONObject;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.TimeZone;

import timber.log.Timber;

public class Util {

    private static DecimalFormat sDecimalFormat = new DecimalFormat("###,###,###,###");

    public static String removeLeadingZero(String value) {
        return value.replaceFirst("^0+(?!$)", "");
    }

    public static int pxToDp(Context context, int px) {
        float widthInDp = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, px, context.getResources().getDisplayMetrics());
        return (int)widthInDp;
    }

    public static String parseJsonString(JSONObject object, String fieldName) {
        String value = "";
        if (object.has(fieldName)) {
            try {
                Object data = object.get(fieldName);
                if (data instanceof Boolean) {
                    return value;
                }
                value = object.getString(fieldName);
            }
            catch (JSONException e) {
                L.e("parseJsonString - JSONException - " + Log.getStackTraceString(e));
            }
        }
        return value;
    }

    public static long parseJsonLong(JSONObject object, String fieldName) {
        long value = 0;
        if (object.has(fieldName)) {
            try {
                value = object.getLong(fieldName);
            }
            catch (JSONException e) {
                e.printStackTrace();
                Timber.e("Error in parseJsonInt");
            }
        }
        return value;
    }

    public static int parseJsonInt(JSONObject object, String fieldName) {
        int value = 0;
        if (object.has(fieldName)) {
            try {
                value = object.getInt(fieldName);
            }
            catch (JSONException e) {
                L.e("parseJsonInt - JSONException - " + Log.getStackTraceString(e));
            }
        }
        return value;
    }

    public static double parseJsonDouble(JSONObject object, String fieldName) {
        double value = 0.0;
        if (object.has(fieldName)) {
            try {
                value = object.getDouble(fieldName);
            }
            catch (JSONException e) {
                e.printStackTrace();
                Timber.e("Error in parseJsonInt");
            }
        }
        return value;
    }

    public static boolean parseJsonBoolean(JSONObject object, String fieldName) {
        boolean value = false;
        if (object.has(fieldName)) {
            try {
                if (object.getString(fieldName).equals("true") || object.getString(fieldName).equals("false")) {
                    value = object.getBoolean(fieldName);
                }
                else {
                    value = object.getString(fieldName).equals("1");
                }
            }
            catch (JSONException e) {
                L.e("parseJsonBoolean - JSONException - " + Log.getStackTraceString(e));
            }
        }
        return value;
    }

    public static String convertTimeStamp2String (long timestamp, String dateFormat) {
        Date date = new Date(timestamp*1000L);
        SimpleDateFormat sdf = new SimpleDateFormat(dateFormat);
        sdf.setTimeZone(TimeZone.getTimeZone("GMT+8"));
        return sdf.format(date);
    }

    public static String encodeString2UrlString (String urlString) {
        try {
            URL url = new URL(urlString);
            URI uri = new URI(url.getProtocol(), url.getUserInfo(), url.getHost(), url.getPort(), url.getPath(), url.getQuery(), url.getRef());
            url = uri.toURL();
            return url.toString();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }

        return null;
    }

    public static Intent getSocialSharingIntent(String url) {
        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_SEND);
        intent.setType("text/plain");
        intent.putExtra(Intent.EXTRA_TEXT, url);

        return Intent.createChooser(intent, "Share");
    }

    public static Intent getAgentSmsIntent(String agentName, String agentContact, String siteUrl) {
        String uri = "sms:" + agentContact;
        Intent sendIntent = new Intent(Intent.ACTION_VIEW);
        sendIntent.setData(Uri.parse(uri));
        sendIntent.setType("vnd.android-dir/mms-sms");
        sendIntent.putExtra("address", agentContact);
        sendIntent.putExtra("sms_body", "Hi " + agentName + ", \n\nI am interested in this property " + siteUrl);

        return sendIntent;
    }

    public static Intent getAgentEmailIntent(String agentName, String agentEmail, String siteUrl) {
        Intent email = new Intent(Intent.ACTION_SEND);
        email.setType("message/rfc822");
        email.putExtra(Intent.EXTRA_EMAIL, new String[]{agentEmail});
        email.putExtra(Intent.EXTRA_SUBJECT, "TEP Interested in this Property");
        email.putExtra(Intent.EXTRA_TEXT, "Hi " + agentName + ", \n\nI am interested in this property " + siteUrl);

        return Intent.createChooser(email, "Send mail...");
    }

    public static <T> ArrayList<T> deepCopyArrayList(ArrayList<T> source, int firstIndex) {
        ArrayList<T> dest = new ArrayList<T>();

        for (int i = firstIndex; i < source.size(); i++) {
            dest.add(source.get(i));
        }

        return dest;
    }
    
    public static int getAmenitiesIconResourceId(Context context, String type) {
        String filename = "";
        if ("ATMs".equals(type)) {
            filename = "map_atm";
        } else if ("Bank Branches".equals(type)) {
            filename = "map_bank";
        } else if ("MRTs".equals(type)) {
            filename = "map_tram";
        } else if ("LRTs".equals(type)) {
            filename = "map_tram";
        } else if ("Bus Stops".equals(type)) {
            filename = "map_bus";
        } else if ("Groceries".equals(type)) {
            filename = "map_supermarket";
        } else if ("Shopping Malls".equals(type)) {
            filename = "map_deptstore";
        } else if ("Clinics".equals(type)) {
            filename = "map_firstaid";
        } else if ("Dentists".equals(type)) {
            filename = "map_dentist";
        } else if ("Childcare Centres".equals(type)) {
            filename = "map_daycare";
        } else if ("Kindergartens".equals(type)) {
            filename = "map_daycare";
        } else if ("Prominent Hawker Centres".equals(type)) {
            filename = "map_fastfood";
        } else if ("Libraries".equals(type)) {
            filename = "map_bookstore";
        } else if ("Community Clubs".equals(type)) {
            filename = "map_communitycentre";
        } else if ("Sports Clubs".equals(type)) {
            filename = "map_soccer2";
        } else if ("Petrol Stations".equals(type)) {
            filename = "map_gazstation";
        } else if ("National Parks".equals(type)) {
            filename = "map_park";
        } else if ("Schools".equals(type)) {
            filename = "map_university";
        }
        return filename.isEmpty() ? 0 : context.getResources().getIdentifier(filename, "drawable", context.getPackageName());
    }
    
    public static String formatCurrency (int money) {
        return sDecimalFormat.format(money);
    }

    public static String formatCurrency (double money) {
        return sDecimalFormat.format(money);
    }
}