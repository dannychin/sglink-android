package com.intewealth.app.sglink.interfaces;

import android.support.v4.app.Fragment;

/**
 * User: Pavel
 * Date: 31.10.2016
 * Time: 9:52
 */
public abstract class RefreshableFragment extends Fragment {

    public abstract void refresh();

}
