package com.intewealth.app.sglink.config;

/**
 * User: Pavel
 * Date: 26.10.2016
 * Time: 11:18
 */
public interface Preferences {

    String LAST_LAUNCH_TIME = "last_start_time";
    String LAST_MAP_LATITUDE = "last_map_latitude";
    String LAST_MAP_LONGITUDE = "last_map_longitude";
    String LAST_MAP_ZOOM = "last_map_zoom";

}
