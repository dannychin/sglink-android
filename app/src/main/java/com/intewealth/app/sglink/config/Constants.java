package com.intewealth.app.sglink.config;

/**
 * User: Pavel
 * Date: 08.12.2016
 * Time: 18:38
 */

public interface Constants {

    interface BusStop {

        String[] NAMES = {
                "WoodLands Sq",
                "Woodlands Checkpt",
                "Johor Bahru Checkpt",
                "Larkin Ter"
        };

        String[] DESCRIPTIONS = {
                "Woodlands Temp Int",
                "Woodlands Crossing",
                "Johor Bahru",
                "Jln Datin Halimah"
        };

        String[] LATS = {
                "1.4373851958647",
                "1.4446083243645",
                "1.4568440736324",
                "1.4939037928636"
        };

        String[] LONS = {
                "103.78583255758",
                "103.76774882217",
                "103.76785998671",
                "103.74403292148"
        };

    }

}
