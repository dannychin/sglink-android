package com.intewealth.app.sglink.models;

import com.intewealth.app.sglink.Utils.L;
import org.json.JSONObject;

import java.io.Serializable;

/**
 * Created by Danny on 22/11/15
 */
public class Camera implements Serializable {

    /************************************
     * PUBLIC STATIC CONSTANTS
     ************************************/
    public static String ROUTE_CAUSEWAY = "causeway";
    public static String ROUTE_LINK2 = "link2";

    /************************************
     * PRIVATE FIELDS
     ************************************/
    private JSONObject metaData;
    private int cameraImageId;
    private int cameraId;
    private String name;
    private Double lat;
    private Double lon;
    private String imageURL;
    private String summary;
    private String createDate;
    private String distance;
    private String guid;
    private String image;
    private String sampleImage;
    private String route;
    private String label;
    private String date;

    /************************************
     * PUBLIC METHODS
     ************************************/
    @Override
    public String toString() {
        return L.string(
                "id", cameraId,
                "label", label,
                "date", date,
                "imageId", cameraImageId,
                "name", name,
                "lat", lat,
                "lon", lon,
                "imageUrl", imageURL,
                "summary", summary,
                "createdDate", createDate,
                "distance", distance,
                "guid", guid,
                "image", image,
                "sampleImage", sampleImage,
                "route", route
        );

    }

    public Camera(int cameraId) {
        this.cameraId = cameraId;

    }

    public Camera(String name) {
        this.name = name;

    }

    public JSONObject getMetadata() {
        return metaData;
    }

    public void setMetadata(JSONObject metadata) {
        metaData = metadata;
    }

    public int getCameraImageId() {
        return cameraImageId;
    }

    public void setCameraImageId(int cameraImageId) {
        this.cameraImageId = cameraImageId;
    }

    public int getCameraId() {
        return cameraId;
    }

    public void setCameraId(int cameraId) {
        this.cameraId = cameraId;
    }

    public Double getLat() {
        return lat;
    }

    public void setLat(Double lat) {
        this.lat = lat;
    }

    public Double getLon() {
        return lon;
    }

    public void setLon(Double lon) {
        this.lon = lon;
    }

    public String getImageURL() {
        return imageURL;
    }

    public void setImageURL(String imageURL) {
        this.imageURL = imageURL;
    }

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public String getCreateDate() {
        return createDate;
    }

    public void setCreateDate(String createDate) {
        this.createDate = createDate;
    }

    public String getDistance() {
        return distance;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }

    public String getGUID() {
        return guid;
    }

    public void setGUID(String GUID) {
        guid = GUID;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getSampleImage() {
        return sampleImage;
    }

    public void setSampleImage(String sampleImage) {
        this.sampleImage = sampleImage;
    }

    public void setRoute(String route) {
        this.route = route;
    }

    public String getRoute() {
        return route;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getName() {
        return name;
    }

}
