package com.intewealth.app.sglink.Bus;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Bitmap;
import android.os.Debug;
import android.support.annotation.IntegerRes;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.intewealth.app.sglink.R;
import com.intewealth.app.sglink.Utils.L;

/**
 * User: Pavel
 * Date: 07.12.2016
 * Time: 20:14
 */

public class BusStopMarkerView extends FrameLayout {

    /************************************
     * BINDS
     ************************************/
    @BindView(R.id.title)
    TextView title;
    @BindView(R.id.bus_icon)
    View busIcon;

    /************************************
     * PUBLIC METHODS
     ************************************/
    public BusStopMarkerView(Context context) {
        super(context);
        init();
    }

    public BusStopMarkerView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public BusStopMarkerView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    public BusStopMarkerView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init();
    }

    public Bitmap render() {
        L.i("render");
        setDrawingCacheEnabled(true);
        measure(0, 0);
        layout(0, 0, getMeasuredWidth(), getMeasuredHeight());
        buildDrawingCache();
        return getDrawingCache();
    }

    public void setTitle(String title) {
        L.i("setTitle");
        this.title.setText(title);
    }

    public void setBusNumber(String busNumber) {
        L.i("setBusNumber", busNumber);

        if (busNumber.contains("170")){
            busIcon.setBackgroundResource(R.drawable.ic_bus_170);
            invalidate();
        } else if (busNumber.contains("950")){
            busIcon.setBackgroundResource(R.drawable.ic_bus_950);
            invalidate();
        }

    }

    /************************************
     * PRIVATE METHODS
     ************************************/
    private void init() {
        L.i("init");
        inflate(getContext(), R.layout.view_bus_stop_marker, this);
        ButterKnife.bind(this);
    }

}
