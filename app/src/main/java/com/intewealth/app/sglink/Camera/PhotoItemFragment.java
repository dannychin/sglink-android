package com.intewealth.app.sglink.Camera;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.intewealth.app.sglink.CameraImageActivity;
import com.intewealth.app.sglink.R;
import com.intewealth.app.sglink.Utils.AppConstant;
import com.intewealth.app.sglink.Utils.BitmapUtils;
import com.intewealth.app.sglink.Utils.L;
import com.intewealth.app.sglink.config.Extra;
import com.intewealth.app.sglink.managers.ImagesManager;
import com.intewealth.app.sglink.models.Camera;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

/**
 * Created by Danny on 4/9/16
 */
public class PhotoItemFragment extends Fragment {

    /************************************
     * PUBLIC STATIC CONSTANTS
     ************************************/
    public static final DateTimeFormatter DATE_FORMATTER_FROM = DateTimeFormat.forPattern("yyyy dd MM HH:mm:ss");
    public static final DateTimeFormatter DATE_FORMATTER_TO = DateTimeFormat.forPattern("dd/MM/yyyy HH:mm");

    /************************************
     * BINDS
     ************************************/
    @BindView(R.id.traffic_image)
    ImageView trafficImageView;
    @BindView(R.id.description)
    TextView description;
    @BindView(R.id.date)
    TextView date;
    @BindView(R.id.progress)
    ProgressBar progressBar;

    /************************************
     * PRIVATE FIELDS
     ************************************/
    private String imageUrl;
    private String cameraName;

    /************************************
     * PUBLIC METHODS
     ************************************/
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        L.i("onCreateView");

        View view = inflater.inflate(R.layout.fragment_photo_item, container, false);
        ButterKnife.bind(this, view);

        final Camera camera = (Camera) getArguments().getSerializable("data");
        L.i("onCreateView", camera);

        imageUrl = AppConstant.TRAFFIC_IMG_BASE_URL + "/" + camera.getImageURL();
        cameraName = camera.getName();
        L.i("onCreateView - show image", imageUrl);

        // if its on disk - load firstly from the disk
        if (ImagesManager.imageSaved(getContext(), camera)) {
            L.i("onCreateView - image saved");
            Glide.with(getContext())
                    .load(ImagesManager.getImageUri(getContext(), camera))
                    .into(trafficImageView);
        }

        // after that - load from web
        Glide
                .with(getContext())
                .load(imageUrl)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .listener(new RequestListener<String, GlideDrawable>() {
                    @Override
                    public boolean onException(Exception e, String s, Target<GlideDrawable> target, boolean b) {
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(GlideDrawable glideDrawable, String s, Target<GlideDrawable> target, boolean b, boolean b1) {
                        progressBar.setVisibility(View.GONE);
                        new SaveImageTask(camera, BitmapUtils.drawableToBitmap(glideDrawable)).execute();
                        //ImagesManager.saveImage(getContext(), camera, glideDrawable);
                        return false;
                    }
                })
                .into(trafficImageView);

        description.setText(camera.getLabel());
        date.setText(DATE_FORMATTER_TO.print(DATE_FORMATTER_FROM.parseDateTime(camera.getDate())));

        return view;

    }

    @OnClick(R.id.traffic_image)
    public void onTrafficImageClick() {
        L.i("onTrafficImageClick");
        Intent cameraImageIntent = new Intent(getContext(), CameraImageActivity.class);
        cameraImageIntent.putExtra(Extra.IMAGE_URL, imageUrl);
        cameraImageIntent.putExtra(Extra.CAMERA_NAME, cameraName);
        startActivity(cameraImageIntent);
    }

    /************************************
     * PRIVATE CLASSES
     ************************************/
    private class SaveImageTask extends AsyncTask<Object, String, String> {

        /************************************
         * PRIVATE FIELDS
         ************************************/
        private Camera camera;
        private Bitmap picture;

        /************************************
         * PUBLIC METHODS
         ************************************/
        public SaveImageTask(Camera camera, Bitmap picture) {
            this.camera = camera;
            this.picture = picture;
        }

        @Override
        public String doInBackground(Object... objects) {
            ImagesManager.saveImage(getContext(), camera, picture);
            return null;
        }

    }

}
