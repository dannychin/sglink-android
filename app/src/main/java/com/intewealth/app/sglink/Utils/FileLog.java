package com.intewealth.app.sglink.Utils;


import android.os.Environment;
import android.text.TextUtils;
import android.util.Log;

import java.io.*;

public abstract class FileLog {

    public static final String TAG = FileLog.class.getSimpleName();

    protected File mFile;

    /**
     * without path
     */
    protected abstract String getFileName();

    /**
     * @return path of file parent directory on external storage (SD card)
     */
    protected String getParentDirectory() {
        return Environment
                .getExternalStorageDirectory()
                .getPath() + "/"
                + getAppDirectoryName();
    }

    /**
     * do not forget "/" at the end
     */
    protected String getAppDirectoryName() {
        return ".tark/";
    }

    /**
     * if actual file size is bigger than this value then file is recreated.
     * Returns 0 by default, which means we not limit file size.
     *
     * used for example to limit size of log file, which has a lot of logs during development
     * @return the maximum number of bytes in this file.
     */
    protected long getFileMaxSize() {
        return 0;
    }

    protected void checkRecreateFile() {
        if(!isExist()) {
            createFile();
            return;
        }

        if (mFile.length() >= getFileMaxSize()
                && mFile.delete()) {
            createFile();
        }
    }

    public boolean createFile() {
        try {
            return ensureParentDirectory()
                    && ensureFile();

        } catch (IOException e) {
            Log.e(TAG, "failed while creating log file");
            return false;
        }
    }

    protected boolean ensureParentDirectory() {
        File parentDirectory = new File(getParentDirectory());
        return parentDirectory.exists()
                || parentDirectory.mkdirs();
    }

    protected boolean ensureFile() throws IOException {
        String fullFileName = getParentDirectory() + getFileName();
        mFile = new File(fullFileName);

        return mFile.exists() || mFile.createNewFile();
    }

    protected boolean isExist() {
        return mFile != null && mFile.exists();
    }

    public void writeFile(String line, boolean append) {
        if (TextUtils.isEmpty(line)) {
            return;
        }

        checkRecreateFile();
        FileOutputStream fos = null;

        try {
            fos = new FileOutputStream(mFile, append);
            fos.write(line.getBytes());

            fos.flush();
            fos.close();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (fos != null) {
                    fos.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public String readFile() {
        BufferedReader br = null;

        try {
            br = new BufferedReader(new FileReader(mFile));
            StringBuilder builder = new StringBuilder("");
            String buffer;

            while ((buffer = br.readLine()) != null) {
                builder.append(buffer);
            }

            return builder.toString();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        } finally {
            try {
                if (br != null) {
                    br.close();
                }
            } catch (Exception exc) {
                exc.printStackTrace();
            }
        }
    }

}