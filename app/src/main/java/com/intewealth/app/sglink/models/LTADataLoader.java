package com.intewealth.app.sglink.models;

import android.net.Uri;
import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.intewealth.app.sglink.Utils.App;
import com.intewealth.app.sglink.Utils.L;
import com.intewealth.app.sglink.Utils.Util;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import timber.log.Timber;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Danny on 22/11/15
 */
public class LTADataLoader extends DataLoader{

    /************************************
     * PRIVATE FIELDS
     ************************************/
    private String mBaseUrl = "http://datamall.mytransport.sg/ltaodataservice.svc/CameraImageSet";
    private String mAcceptHeader = "application/json";
    private String mREQUEST_TAG = "LTA_CAMERA";
    private String mDataTag = "d";
    private ArrayList<Camera> mCameras;
    private Uri mDestinationUri;

    /************************************
     * PUBLIC METHODS
     ************************************/
    public void loadTrafficCamera(){
        L.i("loadTrafficCamera");
        JsonObjectRequest jsonRequest = new JsonObjectRequest(Request.Method.GET, mBaseUrl, "",
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        L.i("onResponse", response);
                        processTrafficResult(response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                        Timber.d("error:" + error.toString());
                    }
                }
        ){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String>  params = new HashMap<String, String>();
                params.put("accept", mAcceptHeader);
                params.put("Content-Type", "application/x-www-form-urlencoded");
                params.put("AccountKey", "Ee8N7ufmJjLJ5E2382V7dg==");
                params.put("UniqueUserID", "e61f60ae-7cf4-45cf-ad3b-ee46dbf61637");
                L.i("getHeaders", params.toString());
                return params;
            }

        };
        App.addRequest(jsonRequest, mREQUEST_TAG);
    }

    public void processTrafficResult(JSONObject jsonResponse){
        L.i("processTrafficResult", jsonResponse.toString());
        //Timber.d(jsonResponse.toString());

        try {
            mCameras = new ArrayList<>();
            JSONArray data = (JSONArray) jsonResponse.get(mDataTag);
            L.i("processTrafficResult", "d length\" + data.length()");
            for(int i =0; i < data.length(); i++){
                JSONObject cameraData = (JSONObject) data.get(i);
                Camera camera = new Camera(Util.parseJsonInt(cameraData,"CameraID"));
                camera.setCameraImageId(Util.parseJsonInt(cameraData,"CameraImageID"));
                camera.setLat(Util.parseJsonDouble(cameraData, "Latitude"));
                camera.setLon(Util.parseJsonDouble(cameraData, "Longtitude"));
                camera.setImageURL(Util.parseJsonString(cameraData, "ImageURL"));

                mCameras.add(camera);

            }
            Timber.d("LTA pre post mcamerars" + mCameras.toString());

        }catch (JSONException jsone){
            Timber.e(jsone.toString());

        }finally {
            L.i("processTrafficResult","LTA post mcamerars" + mCameras.toString());
            App.getInstance().getOttoBus().post(mCameras);
        }
    }

}
