package com.intewealth.app.sglink.config;

/**
 * User: Pavel
 * Date: 11.01.2017
 * Time: 16:13
 */

public interface Extra {

    String IMAGE_URL = "extra_image_url";
    String CAMERA_NAME = "extra_camera_name";

}
