package com.intewealth.app.sglink.com;

import android.content.Context;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.MotionEvent;
import com.intewealth.app.sglink.Utils.L;

/**
 * Created by Danny on 6/9/16
 */
public class FreezeViewPager extends ViewPager {

    /************************************
     * PRIVATE FIELDS
     ************************************/
    private boolean enabled;

    /************************************
     * PUBLIC METHODS
     ************************************/
    public FreezeViewPager(Context context, AttributeSet attrs) {
        super(context, attrs);
        L.i("FreezeViewPager");
        this.enabled = false;
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        L.i("onTouchEvent");
        if (this.enabled) {
            return super.onTouchEvent(event);
        }
        return false;
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent event) {
        L.i("onInterceptTouchEvent");
        if (this.enabled) {
            return super.onInterceptTouchEvent(event);
        }
        return false;
    }

    public void setPagingEnabled(boolean enabled) {
        L.i("setPagingEnabled");
        this.enabled = enabled;
    }

}