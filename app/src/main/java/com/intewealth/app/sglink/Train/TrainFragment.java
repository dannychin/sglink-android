package com.intewealth.app.sglink.Train;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.intewealth.app.sglink.R;
import com.intewealth.app.sglink.Utils.L;

/**
 * Created by Danny on 31/8/16
 */
public class TrainFragment extends Fragment {

    /************************************
     * PUBLIC METHODS
     ************************************/
    public TrainFragment() {
        L.i("TrainFragment");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        L.i("onCreateView");
        return inflater.inflate(R.layout.fragment_bus, container, false);
    }

}
