package com.intewealth.app.sglink.view;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.TypedArray;
import android.os.Build;
import android.util.AttributeSet;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.intewealth.app.sglink.R;
import com.intewealth.app.sglink.Utils.L;

/**
 * User: Pavel
 * Date: 07.12.2016
 * Time: 20:41
 */

public class BusStopItemView extends RelativeLayout {

    /*************************************
     * BINDS
     *************************************/
    @BindView(R.id.title)
    TextView title;
    @BindView(R.id.desc)
    TextView desc;

    /*************************************
     * PRIVATE FIELDS
     *************************************/
    private String titleString;
    private String descriptionString;
    private int mPosition;

    /*************************************
     * PUBLIC METHODS
     *************************************/
    public BusStopItemView(Context context) {
        super(context);
        init(context);
    }

    public BusStopItemView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    public BusStopItemView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public BusStopItemView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init(context, attrs);
    }

    public void setTitle(String title) {
        titleString = title;
        if (this.title != null) {
            this.title.setText(titleString);
        }
    }

    public void setDescription(String description) {
        descriptionString = description;
        if (this.desc != null) {
            this.desc.setText(descriptionString);
        }
    }

    public void setTitleColorResId(int colorResId) {
        L.i("setTitleColorResId", colorResId);
        if (this.title != null) {
            this.title.setTextColor(getResources().getColor(colorResId));
        }
    }

    /*************************************
     * PRIVATE METHODS
     *************************************/
    private void init(Context context) {
        //LLog.e(TAG, "init");
        inflate(context, R.layout.view_list_item, this);
        ButterKnife.bind(this, this);
        setClickable(true);

        if (titleString != null && !titleString.equals("")) {
            this.title.setText(titleString);
        }

        /*if (titleString != null && !titleString.equals("")) {
            this.title.setText(titleString);
        }*/

    }

    private void init(Context context, AttributeSet attributes) {
        //LLog.e(TAG, "init attrs");
        init(context);

        TypedArray styledAttributes = getContext().obtainStyledAttributes(attributes, R.styleable.ListItemView);
        int attrsCount = styledAttributes.getIndexCount();

        for (int i = 0; i < attrsCount; i++) {
            int styledAttributesIndex = styledAttributes.getIndex(i);

            switch (styledAttributesIndex) {
                case R.styleable.ListItemView_itemTitle:
                    title.setText(styledAttributes.getString(styledAttributesIndex));
                    break;
                case R.styleable.ListItemView_itemDesc:
                    desc.setText(styledAttributes.getString(styledAttributesIndex));
                    break;
            }
        }

        styledAttributes.recycle();
    }

}
