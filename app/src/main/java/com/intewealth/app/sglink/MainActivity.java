package com.intewealth.app.sglink;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.renderscript.RSRuntimeException;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.common.api.GoogleApiClient;
import com.intewealth.app.sglink.Bus.BusFragment;
import com.intewealth.app.sglink.Camera.CameraFragment;
import com.intewealth.app.sglink.Utils.L;
import com.intewealth.app.sglink.config.Preferences;
import com.intewealth.app.sglink.config.Settings;
import com.intewealth.app.sglink.interfaces.RefreshableFragment;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    /************************************
     * PUBLIC STATIC CONSTANTS
     ************************************/
    public static final long MAX_TIME_FROM_LAST_LAUNCH = 1000 * 60 * 5;

    /************************************
     * BINDS
     ************************************/
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.pager)
    ViewPager pager;
    @BindView(R.id.tabs)
    TabLayout tabLayout;
    @BindView(R.id.splash_view)
    ImageView splashView;
    @BindView(R.id.app_bar_layout)
    AppBarLayout appBarLayout;

    /************************************
     * PRIVATE FIELDS
     ************************************/
    private GoogleApiClient client;
    private ViewPagerAdapter adapter;
    private Runnable hideSplashRunnable;
    private Handler hideSplashHandler;

    /************************************
     * PUBLIC METHODS
     ************************************/
    @Override
    public void onCreate(Bundle savedInstanceState) {
        L.i("onCreate");

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);

        setupViewPager(pager);
        tabLayout.setupWithViewPager(pager);
        client = new GoogleApiClient.Builder(this).addApi(AppIndex.API).build();

        splashView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                return true;
            }
        });

        hideSplashRunnable = new Runnable() {
            @Override
            public void run() {
                AlphaAnimation fadeOut = (AlphaAnimation) AnimationUtils.loadAnimation(getApplicationContext(), R.anim.fade_out_animation);
                fadeOut.setAnimationListener(new Animation.AnimationListener() {
                    @Override
                    public void onAnimationStart(Animation animation) {

                    }

                    @Override
                    public void onAnimationEnd(Animation animation) {
                        splashView.setVisibility(View.GONE);
                    }

                    @Override
                    public void onAnimationRepeat(Animation animation) {

                    }
                });
                splashView.startAnimation(fadeOut);
                appBarLayout.setVisibility(View.VISIBLE);
            }
        };
        hideSplashHandler = new Handler();
        hideSplashHandler.postDelayed(hideSplashRunnable, 2000);

    }

    @Override
    protected void onDestroy() {
        L.i("onDestroy");
        super.onDestroy();
        hideSplashHandler.removeCallbacks(hideSplashRunnable);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        L.i("onCreateOptionsMenu");
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        L.i("onOptionsItemSelected");
        switch (item.getItemId()) {
            case R.id.action_share:
                share();
                return true;
            case R.id.action_refresh:
                adapter.refresh(true);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onStart() {
        L.i("onStart");
        super.onStart();
        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client.connect();
        Action viewAction = Action.newAction(
                Action.TYPE_VIEW,
                "Main Page",
                Uri.parse("http://host/path"),
                Uri.parse("android-app://com.intewealth.app.sglink/http/host/path")
        );
        AppIndex.AppIndexApi.start(client, viewAction);

        if (tooMuchTimeFromLastLaunch()) {
            adapter.refresh(false);
        }

    }

    @Override
    public void onStop() {
        L.i("onStop");
        super.onStop();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        Action viewAction = Action.newAction(
                Action.TYPE_VIEW, // TODO: choose an action type.
                "Main Page", // TODO: Define a title for the content shown.
                // TODO: If you have web page content that matches this app activity's content,
                // make sure this auto-generated web page URL is correct.
                // Otherwise, set the URL to null.
                Uri.parse("http://host/path"),
                // TODO: Make sure this auto-generated app URL is correct.
                Uri.parse("android-app://com.intewealth.app.sglink/http/host/path")
        );
        AppIndex.AppIndexApi.end(client, viewAction);
        client.disconnect();
        Settings.putLong(Preferences.LAST_LAUNCH_TIME, System.currentTimeMillis());

    }

    /************************************
     * PRIVATE METHODS
     ************************************/
    private void setupViewPager(ViewPager viewPager) {
        L.i("setupViewPager");
        adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new CameraFragment(), "Traffic Cam");
        adapter.addFragment(new BusFragment(), "Bus");
        viewPager.setAdapter(adapter);
        viewPager.setOffscreenPageLimit(3);
    }

    private void share() {
        L.i("share");
        Intent sendIntent = new Intent();
        sendIntent.setType("text/plain");
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_SUBJECT, getString(R.string.share_subject));
        sendIntent.putExtra(Intent.EXTRA_TEXT, getString(R.string.share_body));
        sendIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(Intent.createChooser(sendIntent, getString(R.string.share_window_title)));
    }

    private boolean tooMuchTimeFromLastLaunch() {
        boolean tooMuch = (System.currentTimeMillis() - Settings.getLong(Preferences.LAST_LAUNCH_TIME)) > MAX_TIME_FROM_LAST_LAUNCH;
        L.i("tooMuchTimeFromLastLaunch", tooMuch);
        return tooMuch;
    }

    /************************************
     * PUBLIC CLASSES
     ************************************/
    class ViewPagerAdapter extends FragmentPagerAdapter {

        /************************************
         * PRIVATE FIELDS
         ************************************/
        private final List<RefreshableFragment> fragments = new ArrayList<>();
        private final List<String> titles = new ArrayList<>();

        /************************************
         * PUBLIC METHODS
         ************************************/
        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return fragments.get(position);
        }

        @Override
        public int getCount() {
            return fragments.size();
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return titles.get(position);
        }

        public void addFragment(RefreshableFragment fragment, String title) {
            L.i("addFragment", title);
            fragments.add(fragment);
            titles.add(title);
        }

        public void refresh(boolean withVisibleProgress) {
            L.i("refresh");
            for (RefreshableFragment f : fragments) {
                f.refresh();
            }
        }

    }

}