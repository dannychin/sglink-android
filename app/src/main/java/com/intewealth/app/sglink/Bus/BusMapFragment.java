package com.intewealth.app.sglink.Bus;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.widget.Toast;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.maps.android.clustering.Cluster;
import com.google.maps.android.clustering.ClusterItem;
import com.google.maps.android.clustering.ClusterManager;
import com.google.maps.android.clustering.view.DefaultClusterRenderer;
import com.intewealth.app.sglink.R;
import com.intewealth.app.sglink.Utils.App;
import com.intewealth.app.sglink.Utils.L;
import com.intewealth.app.sglink.config.Preferences;
import com.intewealth.app.sglink.config.Settings;
import com.intewealth.app.sglink.models.Bus;
import com.intewealth.app.sglink.models.BusDataLoader;
import com.intewealth.app.sglink.models.BusStop;
import com.squareup.otto.Subscribe;

import java.util.ArrayList;
import java.util.List;

/**
 * User: Pavel
 * Date: 06.12.2016
 * Time: 13:26
 */

public class BusMapFragment extends SupportMapFragment implements
        OnMapReadyCallback,
        GoogleMap.OnMarkerClickListener,
        GoogleMap.OnCameraChangeListener,
        ClusterManager.OnClusterClickListener<BusStopItem>,
        ClusterManager.OnClusterInfoWindowClickListener<BusStopItem>,
        ClusterManager.OnClusterItemClickListener<BusStopItem>,
        ClusterManager.OnClusterItemInfoWindowClickListener<BusStopItem>,
        GoogleMap.OnMapClickListener {

    /************************************
     * PRIVATE FIELDS
     ************************************/
    private GoogleMap map;
    private List<Bus> buses = new ArrayList<>();
    private LatLng location;
    private ClusterManager<BusStopItem> clusterManager;
    private OnBusMapTouchListener listener;

    /************************************
     * PUBLIC METHODS
     ************************************/
    @Override
    public void onResume() {
        L.i("onResume");
        super.onResume();
        App.getInstance().getOttoBus().register(this);
    }

    @Override
    public void onPause() {
        L.i("onPause");
        super.onPause();
        App.getInstance().getOttoBus().unregister(this);

        /**
         * here we need to remember the lat/lng and zoom
         */
        if (map != null) {
            CameraPosition cp = map.getCameraPosition();
            Settings.putFloat(Preferences.LAST_MAP_LATITUDE, (float) cp.target.latitude);
            Settings.putFloat(Preferences.LAST_MAP_LONGITUDE, (float) cp.target.longitude);
            Settings.putFloat(Preferences.LAST_MAP_ZOOM, cp.zoom);
            L.i("onPause", cp.target.latitude, cp.target.longitude, cp.zoom);
        } else {
            L.i("onPause", "map is NULL");
        }

    }

    @Override
    public void onMapReady(GoogleMap map) {
        L.i("onMapReady");

        this.map = map;

        // check for saved location from the previous launch
        LatLng target;
        float lastLatitude = Settings.getFloat(Preferences.LAST_MAP_LATITUDE, -1);
        float lastLongitude = Settings.getFloat(Preferences.LAST_MAP_LONGITUDE, -1);
        if (lastLatitude != -1 && lastLongitude != -1) {
            target = new LatLng(lastLatitude, lastLongitude);
        } else {
            target = location;
        }

        CameraPosition cameraPosition = new CameraPosition.Builder()
                .target(target)
                .zoom(Settings.getFloat(Preferences.LAST_MAP_ZOOM, 14f))
                .build();

        CameraUpdate cameraUpdate = CameraUpdateFactory.newCameraPosition(cameraPosition);
        map.moveCamera(cameraUpdate);
        map.setOnCameraChangeListener(this);
        map.setOnMarkerClickListener(this);
        map.setOnMapClickListener(this);

        BusDataLoader.getInstance().loadBuses();

    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        L.i("onMarkerClick");
        return true;
    }

    @Override
    public void onCameraChange(CameraPosition cameraPosition) {
        L.i("onCameraChange");
        if (listener != null) {
            listener.onBusMapFragmentTouch();
        }
    }

    @Subscribe
    public void onBusesReceived(ArrayList<Bus> buses) {
        L.i("onBusesReceived", buses.size());
        this.buses = getUniqueBuses(buses);
        initCluster();
    }

    @Override
    public boolean onClusterClick(Cluster<BusStopItem> cluster) {

        // Show a toast with some info when the cluster is clicked.

        Toast.makeText(getContext(), cluster.getSize() + " bus stops", Toast.LENGTH_SHORT).show();

        // Zoom in the cluster. Need to create LatLngBounds and including all the cluster items
        // inside of bounds, then animate to center of the bounds.

        // Create the builder to collect all essential cluster items for the bounds.
        LatLngBounds.Builder builder = LatLngBounds.builder();
        for (ClusterItem item : cluster.getItems()) {
            builder.include(item.getPosition());
        }
        // Get the LatLngBounds
        final LatLngBounds bounds = builder.build();

        // Animate camera to the bounds
        try {
            map.animateCamera(CameraUpdateFactory.newLatLngBounds(bounds, 100));
        } catch (Exception e) {
            e.printStackTrace();
        }

        return true;
    }

    @Override
    public void onClusterInfoWindowClick(Cluster<BusStopItem> cluster) {
        // Does nothing, but you could go to a list of the users.
    }

    @Override
    public boolean onClusterItemClick(BusStopItem item) {
        // Does nothing, but you could go into the user's profile page, for example.
        return false;
    }

    @Override
    public void onClusterItemInfoWindowClick(BusStopItem item) {
        // Does nothing, but you could go into the user's profile page, for example.
    }

    @Override
    public void onMapClick(LatLng latLng) {
        L.i("onMapClick");
        if (listener != null) {
            listener.onBusMapFragmentTouch();
        }
    }

    public void setLocation(LatLng location) {
        L.i("setLocation", location);
        this.location = location;
    }

    public void showBusStop(BusStop busStop) {
        L.i("showBusStop");
        LatLng latLng = new LatLng(busStop.getLatitude(), busStop.getLongitude());
        CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(latLng, 17);
        map.animateCamera(cameraUpdate);
    }

    public void moveTo(double latitude, double longitude) {
        L.i("showBusStop");
        LatLng latLng = new LatLng(latitude, longitude);
        CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(latLng, 17);
        map.animateCamera(cameraUpdate);
    }

    public void setOnBusMapTouchListener(OnBusMapTouchListener listener) {
        this.listener = listener;
    }

    /************************************
     * PRIVATE METHODS
     ************************************/
    private List<Bus> getUniqueBuses(List<Bus> buses) {
        L.i("getUniqueBuses");

        List<Bus> uniqueBuses = new ArrayList<>();

        String prevBusStopId = "";
        for (Bus bus : buses) {
            if (!prevBusStopId.equals(bus.busStopId)) {
                uniqueBuses.add(bus);
                prevBusStopId = bus.busStopId;
            }
        }

        return uniqueBuses;

    }

    private void initCluster() {
        L.i("initCluster");

        clusterManager = new ClusterManager<>(getContext(), map);
        clusterManager.setRenderer(new BusStopRenderer());
        map.setOnCameraIdleListener(clusterManager);
        map.setOnMarkerClickListener(clusterManager);
        map.setOnInfoWindowClickListener(clusterManager);
        clusterManager.setOnClusterClickListener(this);
        clusterManager.setOnClusterInfoWindowClickListener(this);
        clusterManager.setOnClusterItemClickListener(this);
        clusterManager.setOnClusterItemInfoWindowClickListener(this);

        addItems();
        clusterManager.cluster();

    }

    private void addItems() {
        L.i("addItems");
        for (Bus bus : this.buses) {
            clusterManager.addItem(new BusStopItem(
                    bus.getLatitude(),
                    bus.getLongitude(),
                    "",
                    bus.serviceNumber
            ));
        }
    }

    /************************************
     * PRIVATE CLASSES
     ************************************/
    private class BusStopRenderer extends DefaultClusterRenderer<BusStopItem> {

        /************************************
         * PRIVATE final FIELDS
         ************************************/
        private final BusStopMarkerView busStopMarkerView;
        private final BusStopMarkerClusterView busStopMarkerClusterView;

        /************************************
         * PUBLIC METHODS
         ************************************/
        public BusStopRenderer() {
            super(getContext(), map, clusterManager);
            busStopMarkerView = new BusStopMarkerView(getContext());
            busStopMarkerClusterView = new BusStopMarkerClusterView(getContext());
        }

        @Override
        protected void onBeforeClusterItemRendered(BusStopItem busStop, MarkerOptions markerOptions) {
            L.i("onBeforeClusterItemRendered");
            busStopMarkerView.setBusNumber(busStop.getSnippet());
            busStopMarkerView.setTitle(busStop.getTitle());
            markerOptions.icon(BitmapDescriptorFactory.fromBitmap(busStopMarkerView.render()));
        }

        @Override
        protected void onBeforeClusterRendered(Cluster<BusStopItem> cluster, MarkerOptions markerOptions) {
            L.i("onBeforeClusterRendered", cluster.getSize());
            busStopMarkerClusterView.setTitle(String.valueOf(cluster.getSize()));
            markerOptions.icon(BitmapDescriptorFactory.fromBitmap(busStopMarkerClusterView.render()));
        }

        @Override
        protected boolean shouldRenderAsCluster(Cluster cluster) {
            // that mean we never need clustersization
            return cluster.getSize() > 1000;
        }

    }

    /************************************
     * PUBLIC INTERFACES
     ************************************/
    public interface OnBusMapTouchListener {

        void onBusMapFragmentTouch();

    }

}
