package com.intewealth.app.sglink;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.View;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.intewealth.app.sglink.Utils.L;
import com.intewealth.app.sglink.config.Extra;
import com.intewealth.app.sglink.managers.ImagesManager;
import com.intewealth.app.sglink.view.TouchImageView;

import static java.security.AccessController.getContext;

public class CameraImageActivity extends Activity {

    /************************************
     * BINDS
     ************************************/
    @BindView(R.id.img)
    TouchImageView image;

    /************************************
     * PRIVATE FIELDS
     ************************************/
    SimpleTarget<Bitmap> touchImageTarget;

    /************************************
     * PUBLIC METHODS
     ************************************/
    @Override
    public void onCreate(Bundle savedInstanceState) {
        L.i("onCreate");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_camera_image);
        ButterKnife.bind(this);

        Intent data = getIntent();

        if (data.hasExtra(Extra.IMAGE_URL) && data.hasExtra(Extra.CAMERA_NAME)) {

            String imageUrl = data.getStringExtra(Extra.IMAGE_URL);
            String cameraName = data.getStringExtra(Extra.CAMERA_NAME);
            L.i("onCreate", imageUrl, cameraName);

            touchImageTarget = new SimpleTarget<Bitmap>() {
                @Override
                public void onResourceReady(Bitmap resource, GlideAnimation<? super Bitmap> glideAnimation) {
                    L.i("onCreate - onResourceReady");
                    image.setVisibility(View.VISIBLE);
                    image.setImageBitmap(resource);
                    image.setZoom(1);
                }
            };

            // if its on disk - load firstly from the disk
            if (ImagesManager.imageSaved(this, cameraName)) {
                L.i("onCreate - image saved");
                Glide.with(this)
                        .load(ImagesManager.getImageUri(this, cameraName))
                        .asBitmap()
                        .into(touchImageTarget);
            }

            // after that - load from web
            Glide
                    .with(this)
                    .load(imageUrl)
                    .asBitmap()
                    .into(touchImageTarget);

        }

    }

}
