package com.intewealth.app.sglink.Bus;

import android.Manifest;
import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Color;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewPropertyAnimator;
import android.view.ViewTreeObserver;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import butterknife.BindView;
import butterknife.BindViews;
import butterknife.ButterKnife;
import butterknife.OnClick;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.intewealth.app.sglink.R;
import com.intewealth.app.sglink.Utils.L;
import com.intewealth.app.sglink.config.Constants;
import com.intewealth.app.sglink.config.RequestCode;
import com.intewealth.app.sglink.interfaces.RefreshableFragment;
import com.intewealth.app.sglink.models.BusStop;
import com.intewealth.app.sglink.view.BusStopItemView;

import static android.R.attr.duration;
import static android.R.attr.maxHeight;
import static android.view.FrameMetrics.ANIMATION_DURATION;

/**
 * Created by Danny on 31/8/16
 */
public class BusFragment extends RefreshableFragment implements
        GoogleApiClient.ConnectionCallbacks,
        OnMapReadyCallback,
        GoogleApiClient.OnConnectionFailedListener,
        BusMapFragment.OnBusMapTouchListener {

    /************************************
     * PUBLIC STATIC CONSTANTS
     ************************************/
    public static final LatLng SINGAPORE_COORDINATES = new LatLng(1.353844, 103.864271);
    public static final int LOCATION_UPDATE_INTERVAL_TIME = 5000;
    public static final float MAP_ZOOM_COEFFICIENT = 17f;
    public static final int PREDEFINED_BUS_STOP_NUMBER = 4;
    public static final int ANIMATION_DURATION = 300;

    /************************************
     * BINDS
     ************************************/
    @BindViews({R.id.bus_stop_0, R.id.bus_stop_1, R.id.bus_stop_2, R.id.bus_stop_3})
    BusStopItemView[] busStopItemViews;
    @BindView(R.id.bus_stops)
    ScrollView busStopsScrollView;
    @BindView(R.id.location)
    FloatingActionButton locationButton;

    /************************************
     * BINDS
     ************************************/
    private Location lastLocation;
    private GoogleApiClient googleApiClient;
    private Location currentLocation;
    private BusMapFragment mapFragment;
    private GoogleMap map;
    private BusStop[] busStops;
    private int scrollViewMaxHeight;
    private int scrollViewMinHeight;
    private ValueAnimator expandMapAnimator;
    private ValueAnimator collapseMapAnimator;
    private boolean animating;

    /************************************
     * PUBLIC METHODS
     ************************************/
    public BusFragment() {
        L.i("BusFragment");
    }

    protected synchronized void buildGoogleApiClient() {
        L.i("buildGoogleApiClient");
        if (googleApiClient == null) {
            googleApiClient = new GoogleApiClient.Builder(getActivity())
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .addApi(LocationServices.API)
                    .build();
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        L.i("onCreateView");
        return inflater.inflate(R.layout.fragment_bus, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        mapFragment = (BusMapFragment) getChildFragmentManager().findFragmentById(R.id.map);
        mapFragment.setOnBusMapTouchListener(this);
        busStopsScrollView.getViewTreeObserver().addOnScrollChangedListener(new ViewTreeObserver.OnScrollChangedListener() {
            @Override
            public void onScrollChanged() {
                collapseMap();
            }
        });
        Resources res = getResources();
        scrollViewMaxHeight = res.getDimensionPixelOffset(R.dimen.bus_screen_bus_list_max_height);
        scrollViewMinHeight = res.getDimensionPixelOffset(R.dimen.bus_screen_bus_list_min_height);
        initAnimators();
        buildGoogleApiClient();
        initBustStopsList();
    }

    @Override
    public void onStart() {
        L.i("onStart");
        super.onStart();
        googleApiClient.connect();
    }

    @Override
    public void onStop() {
        L.i("onStop");
        super.onStop();
        googleApiClient.disconnect();
    }

    @Override
    public void refresh() {

    }

    @Override
    public void onMapReady(GoogleMap map) {
        L.i("onMapReady");

        CameraPosition cameraPosition = new CameraPosition.Builder()
                .target(getLocation())
                .zoom(MAP_ZOOM_COEFFICIENT)
                .build();

        CameraUpdate cameraUpdate = CameraUpdateFactory.newCameraPosition(cameraPosition);
        map.moveCamera(cameraUpdate);
        map.addMarker(new MarkerOptions().position(cameraPosition.target).title("Marker"));

        this.map = map;

    }

    @Override
    public void onConnected(Bundle bundle) {
        L.i("onConnected");
        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            initMap();
        } else {
            ActivityCompat.requestPermissions(
                    getActivity(),
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION},
                    RequestCode.PREMISSION_LOCATION
            );
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        L.i("onRequestPermissionsResult", requestCode, permissions, grantResults);
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case RequestCode.PREMISSION_LOCATION:
                if (grantResults.length == 2
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED
                        && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                    initMap();
                }
                break;
        }
        //lastLocation = LocationServices.FusedLocationApi.getLastLocation(googleApiClient);
        //mapFragment.getMapAsync(this);
    }

    @Override
    public void onConnectionSuspended(int i) {
        L.i("onConnectionSuspended");
        mapFragment.getMapAsync(mapFragment);
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        L.i("onConnectionFailed");
        mapFragment.getMapAsync(mapFragment);
        //busMapView.getMapAsync(busMapView);
    }

    @Override
    public void onBusMapFragmentTouch() {
        expandMap();
    }

    @OnClick(R.id.location)
    public void onLocationButtonClick() {
        L.i("onLocationButtonClick");
        mapFragment.moveTo(getLocation().latitude, getLocation().longitude);
    }

    /************************************
     * PRIVATE METHODS
     ************************************/
    private LatLng getLocation() {
        L.i("getLocation");

        if (lastLocation != null) {
            return new LatLng(lastLocation.getLatitude(), lastLocation.getLongitude());
        } else {
            return SINGAPORE_COORDINATES;
        }

        //return SINGAPORE_COORDINATES;

    }

    private void moveCameraToLocation(Location location) {
        L.i("moveCameraToLocation", location);
        if (map == null) {
            return;
        }
        map.clear();
        map.addMarker(new MarkerOptions()
                .position(new LatLng(location.getLatitude(), location.getLongitude()))
                .title("Marker")
        );
    }

    private void initMap() {
        L.i("initMap");
        try {
            lastLocation = LocationServices.FusedLocationApi.getLastLocation(googleApiClient);
            mapFragment.setLocation(getLocation());
            mapFragment.getMapAsync(mapFragment);
        } catch (SecurityException e) {
            L.e("onRequestPermissionsResult - SecurityException - " + Log.getStackTraceString(e));
        }

    }

    private void initBustStopsList() {
        L.i("initBustStopsList");
        busStops = new BusStop[PREDEFINED_BUS_STOP_NUMBER];
        for (int i = 0; i < PREDEFINED_BUS_STOP_NUMBER; i++) {
            final int ind = i;
            busStops[i] = new BusStop(
                    Constants.BusStop.NAMES[i],
                    Constants.BusStop.DESCRIPTIONS[i],
                    Constants.BusStop.LATS[i],
                    Constants.BusStop.LONS[i]
            );
            busStopItemViews[i].setTitle(busStops[i].name);
            busStopItemViews[i].setDescription(busStops[i].description);
            busStopItemViews[i].setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mapFragment.showBusStop(busStops[ind]);
                }
            });
        }
    }

    private void expandMap() {
        L.i("expandMap");

        if (animating || busStopsScrollView.getLayoutParams().height == scrollViewMinHeight) {
            return;
        }


        expandMapAnimator.start();

    }

    private void collapseMap() {
        L.i("collapseMap");

        if (animating || busStopsScrollView.getLayoutParams().height == scrollViewMaxHeight) {
            return;
        }

        collapseMapAnimator.start();

    }

    private void initAnimators() {
        L.i("initAnimators");

        expandMapAnimator = ValueAnimator.ofInt(scrollViewMaxHeight, scrollViewMinHeight);
        expandMapAnimator.setDuration(ANIMATION_DURATION);
        expandMapAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                int val = (Integer) expandMapAnimator.getAnimatedValue();
                ViewGroup.LayoutParams layoutParams = busStopsScrollView.getLayoutParams();
                layoutParams.height = val;
                busStopsScrollView.setLayoutParams(layoutParams);
            }
        });
        expandMapAnimator.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {
                animating = true;
            }

            @Override
            public void onAnimationEnd(Animator animator) {
                animating = false;
            }

            @Override
            public void onAnimationCancel(Animator animator) {

            }

            @Override
            public void onAnimationRepeat(Animator animator) {

            }
        });

        collapseMapAnimator = ValueAnimator.ofInt(scrollViewMinHeight, scrollViewMaxHeight);
        collapseMapAnimator.setDuration(ANIMATION_DURATION);
        collapseMapAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                int val = (Integer) collapseMapAnimator.getAnimatedValue();
                ViewGroup.LayoutParams layoutParams = busStopsScrollView.getLayoutParams();
                layoutParams.height = val;
                busStopsScrollView.setLayoutParams(layoutParams);
            }
        });
        collapseMapAnimator.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {
                animating = true;
            }

            @Override
            public void onAnimationEnd(Animator animator) {
                animating = false;
            }

            @Override
            public void onAnimationCancel(Animator animator) {

            }

            @Override
            public void onAnimationRepeat(Animator animator) {

            }
        });
    }

}
