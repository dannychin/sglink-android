package com.intewealth.app.sglink.models;

import com.intewealth.app.sglink.Utils.L;

/**
 * User: Pavel
 * Date: 06.12.2016
 * Time: 13:35
 */

public class Bus {

    /************************************
     * PUBLIC STATIC CONSTANTS
     ************************************/
    public static final String DESCRIPTION = "description";
    public static final String STOP_LAT = "stoplat";
    public static final String STOP_LON = "stoplon";
    public static final String BUS_STOP_ID = "busstopid";
    public static final String SERVICE_NUMBER = "serviceno";
    public static final String ESTIMATED_ARRIVAL = "estimatedarrival";
    public static final String LATITUDE = "lat";
    public static final String LONGITUDE = "lon";
    public static final String VISIT_NUMBER = "visitno";
    public static final String SEQUENCE = "seq";

    /************************************
     * PUBLIC FIELDS
     ************************************/
    public String description;
    public String stopLat;
    public String stopLon;
    public String busStopId;
    public String serviceNumber;
    public String estimatedArrival;
    public String latitude;
    public String longitude;
    public String visitNumber;
    public String sequence;

    /************************************
     * PUBLIC METHODS
     ************************************/
    @Override
    public String toString() {
        return L.string(
                "description", description,
                "stopLat", stopLat,
                "stopLon", stopLon,
                "busStopId", busStopId,
                "serviceNumber", serviceNumber,
                "estimatedArrival", estimatedArrival,
                "latitude", latitude,
                "longitude", longitude,
                "visitNumber", visitNumber,
                "sequence", sequence
        );
    }

    public float getLatitude() {
        L.i("getLatitude");
        return Float.parseFloat(latitude);
    }

    public float getLongitude() {
        L.i("getLatitude");
        return Float.parseFloat(longitude);
    }

    public float getStopLatitude() {
        L.i("getLatitude");
        return Float.parseFloat(stopLat);
    }

    public float getStopLongitude() {
        L.i("getLatitude");
        return Float.parseFloat(stopLon);
    }


}
