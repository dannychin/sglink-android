package com.intewealth.app.sglink.Camera;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnPageChange;
import com.intewealth.app.sglink.R;
import com.intewealth.app.sglink.Utils.App;
import com.intewealth.app.sglink.Utils.L;
import com.intewealth.app.sglink.interfaces.RefreshableFragment;
import com.intewealth.app.sglink.models.Camera;
import com.intewealth.app.sglink.models.TrafficCamDataLoader;
import com.squareup.otto.Subscribe;
import com.viewpagerindicator.CirclePageIndicator;

import java.util.ArrayList;
import java.util.HashMap;


public class CameraFragment extends RefreshableFragment {

    /************************************
     * BINDS
     ************************************/
    @BindView(R.id.causeway_pager)
    ViewPager causewayPager;
    @BindView(R.id.causeway_indicator)
    CirclePageIndicator causewayIndicator;
    @BindView(R.id.link2_pager)
    ViewPager link2pager;
    @BindView(R.id.link2_indicator)
    CirclePageIndicator link2Indicator;
    @BindView(R.id.progress_bar_layout_0)
    FrameLayout progressBarLayout0;
    @BindView(R.id.progress_bar_layout_1)
    FrameLayout progressBarLayout1;

    /************************************
     * PRIVATE FIELDS
     ************************************/
    private int causewaySelectedPage = -1;
    private int link2SelectedPage = -1;

    /************************************
     * PUBLIC METHODS
     ************************************/
    public CameraFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        L.i("onCreate");
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onPause() {
        L.i("onPause");
        super.onPause();
        App.getInstance().getOttoBus().unregister(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        L.i("onCreateView");
        View view = inflater.inflate(R.layout.fragment_camera, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onResume() {
        L.i("onResume");
        super.onResume();
        App.getInstance().getOttoBus().register(this);
        TrafficCamDataLoader.getInstance().loadTrafficCamera();
    }

    @Subscribe
    public void onCamerasReceived(HashMap<String, ArrayList<Camera>> cameras) {
        L.i("onBusesReceived", cameras.size());

        ArrayList<Camera> causewayCameras = cameras.get(Camera.ROUTE_CAUSEWAY);
        ArrayList<Camera> link2Cameras = cameras.get(Camera.ROUTE_LINK2);

        if (!getActivity().isFinishing()) {

            progressBarLayout0.setVisibility(View.GONE);
            progressBarLayout1.setVisibility(View.GONE);

            causewayPager.setAdapter(new CameraFragmentAdapter(getFragmentManager(), causewayCameras));
            causewayIndicator.setViewPager(causewayPager);
            if (causewaySelectedPage > 0){
                causewayPager.setCurrentItem(causewaySelectedPage);
            }
            link2pager.setAdapter(new CameraFragmentAdapter(getFragmentManager(), link2Cameras));
            link2Indicator.setViewPager(link2pager);
            if (link2SelectedPage > 0){
                link2pager.setCurrentItem(link2SelectedPage);
            }
        }

    }

    @Override
    public void refresh() {
        L.i("refresh");
        if (progressBarLayout0 != null) {
            progressBarLayout0.setVisibility(View.VISIBLE);
        }
        if (progressBarLayout1 != null) {
            progressBarLayout1.setVisibility(View.VISIBLE);
        }
        TrafficCamDataLoader.getInstance().loadTrafficCamera();
    }

    @OnPageChange(R.id.causeway_pager)
    public void onCausewayPageSelected(int pageNumber) {
        L.i("onCausewayPageSelected");
        causewaySelectedPage = pageNumber;
    }

    @OnPageChange(R.id.link2_pager)
    public void onLink2PageSelected(int pageNumber) {
        L.i("onLink2PageSelected");
        link2SelectedPage = pageNumber;
    }

    /************************************
     * PUBLIC CLASSES
     ************************************/
    public class CameraFragmentAdapter extends FragmentPagerAdapter {

        /************************************
         * PRIVATE FIELDS
         ************************************/
        private ArrayList<Camera> mCameras;

        /************************************
         * PUBLIC METHODS
         ************************************/
        public CameraFragmentAdapter(FragmentManager fm, ArrayList<Camera> cameras) {
            super(fm);
            L.i("CameraFragmentAdapter");
            mCameras = cameras;
        }

        @Override
        public Fragment getItem(int position) {
            try {
                PhotoItemFragment fragment = new PhotoItemFragment();
                Camera camera = mCameras.get(position);
                Bundle args = new Bundle();
                args.putSerializable("data", camera);
                fragment.setArguments(args);
                return fragment;
            } catch (Exception e) {
                L.e("getItem - Exception - " + Log.getStackTraceString(e));
                return null;
            }
        }

        @Override
        public int getCount() {
            return mCameras.size();
        }

        public CharSequence getPageTitle(int position) {
            return "";
        }

    }

}
