package com.intewealth.app.sglink.models;

import com.intewealth.app.sglink.Utils.L;

/**
 * User: Pavel
 * Date: 06.12.2016
 * Time: 13:35
 */

public class BusStop {

    /************************************
     * PUBLIC FIELDS
     ************************************/
    public String name;
    public String description;
    public String lat;
    public String lon;

    /************************************
     * PUBLIC METHODS
     ************************************/
    public BusStop(String name, String description, String lat, String lon) {
        this.name = name;
        this.description = description;
        this.lat = lat;
        this.lon = lon;
    }

    @Override
    public String toString() {
        return L.string(
                "lat", lat,
                "lon", lon,
                "name", name,
                "description", description
        );
    }

    public float getLatitude() {
        L.i("getLatitude");
        return Float.parseFloat(lat);
    }

    public float getLongitude() {
        L.i("getLatitude");
        return Float.parseFloat(lon);
    }

}
